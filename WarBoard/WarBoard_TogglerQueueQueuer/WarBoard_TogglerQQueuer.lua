if not WarBoard_TogglerQQueuer then WarBoard_TogglerQQueuer = {} end
local WarBoard_TogglerQQueuer = WarBoard_TogglerQQueuer
local modName = "WarBoard_TogglerQQueuer"
local modLabel = "Queue"
local initialized, timer = false, 0

function WarBoard_TogglerQQueuer.Initialize()
	if LibWBToggler.CreateToggler(modName, modLabel, "EA_Training_Specialization", 0, 0, "tactic-white", 0.4) then
		WindowSetDimensions(modName, 110, 30)
		WindowSetDimensions(modName.."Label", 60, 30)
		LibWBToggler.RegisterEvent(modName, "OnLButtonUp", "QueueQueuer_GUI.MapButton_OnLButtonUp")
		LibWBToggler.RegisterEvent(modName, "OnRButtonUp", "WarBoard_TogglerQQueuer.MenuDropdown")
		LibWBToggler.RegisterEvent(modName, "OnMouseOver", "WarBoard_TogglerQQueuer.ShowStatus")
		LibWBToggler.RegisterEvent(modName, "OnUpdate", "WarBoard_TogglerQQueuer.OnUpdate")
	end
	WarBoard_TogglerQQueuer.DisableMain()
	initialized = true
end

function WarBoard_TogglerQQueuer.OnUpdate(elapsed)
	if not initialized then return end
	timer = timer + elapsed
	if timer < 1 then return end

	if QueueQueuer_SavedVariables["enabled"] then
		if QueueQueuer.QueueCooldown() then
			DynamicImageSetTextureSlice(modName.."Icon", "tactic-yellow")
			--LabelSetTextColor(modName.."Label", 255, 255, 0)
			LabelSetTextColor(modName.."Label", 255, 255, 255)
		elseif QueueQueuer.IsQueuer() then
			DynamicImageSetTextureSlice(modName.."Icon", "tactic-white")
			LabelSetTextColor(modName.."Label", 255, 255, 255)
		else
			DynamicImageSetTextureSlice(modName.."Icon", "tactic-black")
			--LabelSetTextColor(modName.."Label", 100, 100, 100)
			LabelSetTextColor(modName.."Label", 255, 255, 255)
		end
	else
		DynamicImageSetTextureSlice(modName.."Icon", "tactic-orange")
		LabelSetTextColor(modName.."Label", 255, 165, 0)
	end
	timer = 0
end

function WarBoard_TogglerQQueuer.DisableMain()
	LayoutEditor.UnregisterWindow("QueueQueuer_GUI_MapButtons")
	QueueQueuer_GUI.OnUpdate = nil
	QueueQueuer_GUI.MapButton_OnUpdate = nil
	WindowSetShowing("QueueQueuer_GUI_MapButton", false)
	WindowSetShowing("QueueQueuer_GUI_MapButton_DISABLED", false)
	WindowSetShowing("QueueQueuer_GUI_MapButton_QUEUER", false)
	WindowSetShowing("QueueQueuer_GUI_MapButton_COOLDOWN", false)
end

function WarBoard_TogglerQQueuer.ShowStatus()
	QueueQueuer_GUI.MapButton_OnMouseOver()
	Tooltips.AnchorTooltip(WarBoard.GetModToolTipAnchor(modName))
end

function WarBoard_TogglerQQueuer.MenuDropdown()
	QueueQueuer_GUI.CreateContextMenu(modName)
end
