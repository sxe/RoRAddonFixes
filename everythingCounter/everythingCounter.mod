<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="everythingCounter" version="0.1" date="05/06/2022">
		<Author name="provide" />
		<Description text="Counts reccuring chat events, to keep track of kills, loot and everything else you can see in chat. Run /everythingCounter command for instructions." />
		<VersionSettings gameVersion="1.4.8" windowsVersion="0.1" />
		<Dependencies>
			<Dependency name="LibSlash" optional="false" forceEnable="true" />
		</Dependencies>
		<Files>
			<File name="everythingCounterConstants.lua" />

			<File name="config/everythingCounterCommon.xml" />
			<File name="config/everythingCounterConfigTab.lua" />
			<File name="config/everythingCounterConfigTab.xml" />
			<File name="config/everythingCounterConfigWindow.lua" />
			<File name="config/everythingCounterConfigWindow.xml" />
			<File name="config/everythingCounterCounter.lua" />
			<File name="config/everythingCounterCounter.xml" />

			<File name="everythingCounter.lua" />
			<File name="everythingCounter.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="everythingCounter.Settings" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="everythingCounter.OnInitialize" />
 			<CreateWindow name="everythingCounterConfigWindow" show="false"/>
		</OnInitialize>
		<OnUpdate>
			<CallFunction name="everythingCounter.timerUpdate" />
		</OnUpdate>
		<OnShutdown>
			<CallFunction name="everythingCounter.OnShutdown" />
		</OnShutdown>
		<WARInfo>
			<Categories>
				<Category name="OTHER" />
			</Categories>
		</WARInfo>
	</UiMod>
</ModuleFile>