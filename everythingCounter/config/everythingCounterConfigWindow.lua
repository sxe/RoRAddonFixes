everythingCounterConfigWindow = {}

everythingCounterConfigWindow.name = "everythingCounterConfigWindow"

everythingCounterConfigWindow.SelectedTab = 1

everythingCounterConfigWindow.Tabs = {}
everythingCounterConfigWindow.Tabs[1] = { button = "everythingCounterConfigWindowTabsConfig", name = everythingCounterConfigTab.name,
							label = "Config", show = everythingCounterConfigTab.Show, hide = everythingCounterConfigTab.Hide}

function everythingCounterConfigWindow.Initialize()
	-- Header text
	LabelSetText(everythingCounterConfigWindow.name .. "TitleBarText", L"everythingCounter config")

	-- Tab Text
	everythingCounterConfigWindow.SetTabLabels()

	-- Show SelectedTab by default
	everythingCounterConfigWindow.ShowActiveTab()
end

function everythingCounterConfigWindow.Hide()
	WindowSetShowing(everythingCounterConfigWindow.name, false)
end

function everythingCounterConfigWindow.Show()
	WindowSetShowing(everythingCounterConfigWindow.name, true)
end



---------------------------------------
-- Tab Controls
---------------------------------------

function everythingCounterConfigWindow.SetTabLabels()
	for _, tab in ipairs(everythingCounterConfigWindow.Tabs) do
		ButtonSetText(tab.button, towstring(tab.label))
	end
end

function everythingCounterConfigWindow.ShowActiveTab()
	everythingCounterConfigWindow.HideTabAllContent()
	everythingCounterConfigWindow.Tabs[everythingCounterConfigWindow.SelectedTab].show()
	everythingCounterConfigWindow.SetHighlightedTabText(everythingCounterConfigWindow.SelectedTab)
end

function everythingCounterConfigWindow.OnLButtonUpTab()
	local windowIndex = WindowGetId(SystemData.ActiveWindow.name)
	everythingCounterConfigWindow.SelectedTab = windowIndex
	everythingCounterConfigWindow.ShowActiveTab()
end

function everythingCounterConfigWindow.HideTabAllContent()
	for _, tab in ipairs(everythingCounterConfigWindow.Tabs) do
		tab.hide()
	end
end

function everythingCounterConfigWindow.SetHighlightedTabText(tabNumber)
	for index, tab in ipairs(everythingCounterConfigWindow.Tabs) do
		ButtonSetPressedFlag(tab.button, not(index ~= tabNumber))
	end
end