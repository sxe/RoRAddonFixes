everythingCounterConfigTab = {}

local tinsert = table.insert

everythingCounterConfigTab.name = "everythingCounterConfigTabContent"

function everythingCounterConfigTab.Initialize()

	everythingCounterConfigTab.counterListData = {}
	everythingCounterConfigTab.windowSize = 0
	everythingCounterConfigTab.windowCounterWidth = 0
	everythingCounterConfigTab.windowCounterHeight = 0

	-- Content setup
	LabelSetText(everythingCounterConfigTab.name .. "Label", L"Counter")

	WindowSetTintColor (everythingCounterConfigTab.name.."ListBackground", 100, 100, 100)
	WindowSetAlpha (everythingCounterConfigTab.name.."ListBackground", 0.5)
	ButtonSetText (everythingCounterConfigTab.name.."AddButton", L"Add")
	ButtonSetText (everythingCounterConfigTab.name.."EditButton", L"Edit")
	ButtonSetText (everythingCounterConfigTab.name.."DeleteButton", L"Delete")
	ButtonSetText (everythingCounterConfigTab.name.."UpButton", L"Up")
	ButtonSetText (everythingCounterConfigTab.name.."DownButton", L"Down")

	-- Bottom buttons
	ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", true)
	ButtonSetText(everythingCounterConfigTab.name .. "SaveButton", L"Save")
	ButtonSetText(everythingCounterConfigTab.name .. "ResetButton", L"Reset")

	everythingCounterConfigTab.OnLoad()
end

function everythingCounterConfigTab.Hide()
	WindowSetShowing(everythingCounterConfigTab.name, false)
end
function everythingCounterConfigTab.Show()
	WindowSetShowing(everythingCounterConfigTab.name, true)
end

function everythingCounterConfigTab.OnLoad ()
	everythingCounterConfigTab.isLoaded = false
	everythingCounterConfigTab.CounterListSelectedIndex = nil
	everythingCounterConfigTab.windowSize = everythingCounter.Settings.windowSize
	everythingCounterConfigTab.windowCounterWidth = everythingCounter.Settings.windowCounterWidth
	everythingCounterConfigTab.windowCounterHeight = everythingCounter.Settings.windowCounterHeight
	everythingCounterConfigTab.isLoaded = true

	everythingCounterConfigTab.OnCounterListUpdate()
end

function everythingCounterConfigTab.OnCounterListPopulate ()

	local list = _G[everythingCounterConfigTab.name.."List"]
	if (list.PopulatorIndices == nil) then return end

	local row_window_name
	local data
	local label

	for k, v in ipairs (list.PopulatorIndices)
	do
		row_window_name = everythingCounterConfigTab.name.."ListRow"..k
		data = everythingCounterConfigTab.counterListData[v]

		if (v == everythingCounterConfigTab.CounterListSelectedIndex) then
			WindowSetShowing (row_window_name.."Background", true)
			WindowSetAlpha (row_window_name.."Background", 0.5)
			WindowSetTintColor (row_window_name.."Background", 255, 204, 102)
		else
			WindowSetShowing (row_window_name.."Background", false)
		end

		label = data.label 
		label = label:gsub("<%w+>", " ", 1);
		if (data.isNotEnabled) then
			LabelSetText (row_window_name.."Text", L"[disabled] ".. towstring(label))
		else
			LabelSetText (row_window_name.."Text", towstring(label))
		end
		local color = everythingCounter.Colors[data.labelColor] or everythingCounter.Colors["white"]
		LabelSetTextColor(row_window_name.."Text", color[1], color[2], color[3])

	end
end

function everythingCounterConfigTab.OnCounterListUpdate ()

	if (table.getn(everythingCounterConfigTab.counterListData) == 0) then
		for i=1, #everythingCounter.Settings.counter do
			local data = everythingCounter.Settings.counter[i]
			tinsert (everythingCounterConfigTab.counterListData, data)
		end
	end
	everythingCounterConfigTab.counterListIndexes = {}
	for i=1, #everythingCounterConfigTab.counterListData do
		tinsert (everythingCounterConfigTab.counterListIndexes, i)
	end

	ListBoxSetDisplayOrder (everythingCounterConfigTab.name.."List", everythingCounterConfigTab.counterListIndexes)
end

function everythingCounterConfigTab.OnListLButtonUp ()

	local dataIndex = ListBoxGetDataIndex (everythingCounterConfigTab.name.."List", WindowGetId (SystemData.MouseOverWindow.name))

	if (dataIndex == nil) then
		everythingCounterConfigTab.CounterListSelectedIndex = nil
	else
		everythingCounterConfigTab.CounterListSelectedIndex = everythingCounterConfigTab.counterListIndexes[dataIndex]
	end

	everythingCounterConfigTab.ListSelChanged ()
end

function everythingCounterConfigTab.ListSelChanged ()

	if (not everythingCounterConfigTab.isLoaded) then return end

	local data = everythingCounterConfigTab.counterListData[everythingCounterConfigTab.CounterListSelectedIndex]
	if (not data) then
		everythingCounterConfigTab.CounterListSelectedIndex = nil
	end

	everythingCounterConfigTab.OnCounterListUpdate ()
	ButtonSetDisabledFlag (everythingCounterConfigTab.name.."EditButton", everythingCounterConfigTab.CounterListSelectedIndex == nil)
	ButtonSetDisabledFlag (everythingCounterConfigTab.name.."DeleteButton", everythingCounterConfigTab.CounterListSelectedIndex == nil)
	ButtonSetDisabledFlag (everythingCounterConfigTab.name.."UpButton", everythingCounterConfigTab.CounterListSelectedIndex == nil or everythingCounterConfigTab.CounterListSelectedIndex == 1)
	ButtonSetDisabledFlag (everythingCounterConfigTab.name.."DownButton", everythingCounterConfigTab.CounterListSelectedIndex == nil or everythingCounterConfigTab.CounterListSelectedIndex == #everythingCounterConfigTab.counterListData)
	--ButtonSetDisabledFlag (everythingCounterConfigTab.name.."ExportButton", everythingCounterConfigTab.CounterListSelectedIndex == nil)

	if (data and data.isNotEnabled)
	then
		ButtonSetText (everythingCounterConfigTab.name.."EnableButton", L"Enable")
	else
		ButtonSetText (everythingCounterConfigTab.name.."EnableButton", L"Disable")
	end

	ButtonSetDisabledFlag (everythingCounterConfigTab.name.."EnableButton", everythingCounterConfigTab.CounterListSelectedIndex == nil)
end

function everythingCounterConfigTab.CounterAdd ()

	local obj = EverythingCounterCounter.New ()

	obj:Edit (function (counter)
		tinsert (everythingCounterConfigTab.counterListData, counter)

		everythingCounterConfigTab.CounterListSelectedIndex = #everythingCounterConfigTab.counterListData
		everythingCounterConfigTab.ListSelChanged ()
		ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", false)
	end)
end

function everythingCounterConfigTab.CounterEdit ()

	if (not everythingCounterConfigTab.isLoaded
		or not everythingCounterConfigTab.CounterListSelectedIndex
		or ButtonGetDisabledFlag (everythingCounterConfigTab.name.."EditButton")) then return end

	local data = everythingCounterConfigTab.counterListData[everythingCounterConfigTab.CounterListSelectedIndex]
	local obj = EverythingCounterCounter.New (data)
	obj:Edit (function (counter)
		everythingCounterConfigTab.counterListData[everythingCounterConfigTab.CounterListSelectedIndex] = counter
		everythingCounterConfigTab.ListSelChanged ()
		ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", false)
	end)
end

function everythingCounterConfigTab.ListDelete ()

	if (not everythingCounterConfigTab.isLoaded
		or not everythingCounterConfigTab.CounterListSelectedIndex
		or ButtonGetDisabledFlag (everythingCounterConfigTab.name.."DeleteButton")) then return end

	DialogManager.MakeTwoButtonDialog (
		L"Delete Counter '".. towstring(everythingCounterConfigTab.counterListData[everythingCounterConfigTab.CounterListSelectedIndex].label)..L"' ?",
		L"Yes", 
		function ()
			table.remove (everythingCounterConfigTab.counterListData, everythingCounterConfigTab.CounterListSelectedIndex)
			everythingCounterConfigTab.CounterListSelectedIndex = nil
			everythingCounterConfigTab.ListSelChanged ()
			ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", false)
		end,
		L"No")
end

function everythingCounterConfigTab.ListUp ()

	if (not everythingCounterConfigTab.isLoaded
		or not everythingCounterConfigTab.CounterListSelectedIndex
		or ButtonGetDisabledFlag (everythingCounterConfigTab.name.."UpButton")) then return end

	local index = everythingCounterConfigTab.CounterListSelectedIndex

	local tmp = everythingCounterConfigTab.counterListData[index - 1]
	everythingCounterConfigTab.counterListData[index - 1] = everythingCounterConfigTab.counterListData[index]
	everythingCounterConfigTab.counterListData[index] = tmp

	everythingCounterConfigTab.CounterListSelectedIndex = index - 1
	everythingCounterConfigTab.ListSelChanged()
	ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", false)
end

function everythingCounterConfigTab.ListDown ()

	if (not everythingCounterConfigTab.isLoaded
		or not everythingCounterConfigTab.CounterListSelectedIndex
		or ButtonGetDisabledFlag (everythingCounterConfigTab.name.."DownButton")) then return end

	local index = everythingCounterConfigTab.CounterListSelectedIndex

	local tmp = everythingCounterConfigTab.counterListData[index + 1]
	everythingCounterConfigTab.counterListData[index + 1] = everythingCounterConfigTab.counterListData[index]
	everythingCounterConfigTab.counterListData[index] = tmp

	everythingCounterConfigTab.CounterListSelectedIndex = index + 1
	everythingCounterConfigTab.ListSelChanged()
	ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", false)
end

function everythingCounterConfigTab.ListEnable ()

	if (not everythingCounterConfigTab.isLoaded
		or not everythingCounterConfigTab.CounterListSelectedIndex
		or ButtonGetDisabledFlag (everythingCounterConfigTab.name.."EnableButton")) then return end

	local data = everythingCounterConfigTab.counterListData[everythingCounterConfigTab.CounterListSelectedIndex]
	data.isNotEnabled = not data.isNotEnabled

	everythingCounterConfigTab.ListSelChanged()
	ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", false)
end

function everythingCounterConfigTab.OnMouseOverSaveButton()
	-- just a tooltip
	local anchor = { Point="right",  RelativeTo=SystemData.ActiveWindow.name, RelativePoint="left", XOffset=5, YOffset=0 }
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name )
    Tooltips.SetTooltipText( 1, 1, L"Permanently save the changes made above.")
    Tooltips.Finalize();
    Tooltips.AnchorTooltip( anchor )
end

function everythingCounterConfigTab.OnMouseOverResetButton()
	-- just a tooltip
	local anchor = { Point="right",  RelativeTo=SystemData.ActiveWindow.name, RelativePoint="left", XOffset=5, YOffset=0 }
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name )
    Tooltips.SetTooltipText( 1, 1, L"Reset everything above to the default values.")
    Tooltips.Finalize();
    Tooltips.AnchorTooltip( anchor )
end

function everythingCounterConfigTab.OnResetCounter ()
	DialogManager.MakeTwoButtonDialog (
	L"Reset all counter? This will delete your changes and load the default counter.",
	L"Yes",
	function ()
		everythingCounterConfigTab.counterListData = everythingCounter.DefaultSettings.counter
		everythingCounterConfigTab.windowSize = everythingCounter.DefaultSettings.windowSize
		everythingCounterConfigTab.windowCounterWidth = everythingCounter.DefaultSettings.windowCounterWidth
		everythingCounterConfigTab.windowCounterHeight = everythingCounter.DefaultSettings.windowCounterHeight

		everythingCounterConfigTab.OnCounterListUpdate()
		ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", false)
	end,
	L"No")
end

function everythingCounterConfigTab.OnSaveCounter ()
	DialogManager.MakeTwoButtonDialog (
	L"Save all counter? This makes your changes persistent and cannot be undone.",
	L"Yes",
	function ()
		everythingCounter.Settings.counter = everythingCounterConfigTab.counterListData
		everythingCounter.drawWindows(everythingCounter.Settings.counter);
		everythingCounter.Settings.windowSize = everythingCounter.DefaultSettings.windowSize
		everythingCounter.Settings.windowCounterWidth = everythingCounter.DefaultSettings.windowCounterWidth
		everythingCounter.Settings.windowCounterHeight = everythingCounter.DefaultSettings.windowCounterHeight

		ButtonSetDisabledFlag(everythingCounterConfigTab.name .. "SaveButton", true)
		ModulesSaveSettings() -- Write SavedVariables.lua to disk
	end,
	L"No")
end
