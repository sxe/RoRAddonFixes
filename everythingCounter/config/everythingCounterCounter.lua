local tinsert = table.insert
local tsort = table.sort
local ipairs = ipairs
local pairs = pairs

--WbLeadHelperMessage class
EverythingCounterCounter = {}
EverythingCounterCounter.__index = EverythingCounterCounter

function EverythingCounterCounter.New (data)
	local obj = {}
	setmetatable (obj, EverythingCounterCounter)

	-- defaults
	obj.label = ""
	obj.labelColor = 1
	obj.counter = 0
	obj.counts = 0
	obj.channel = ""
	obj.isNotEnabled = false
	
	if (data)
	then
		obj:Load (data)
	end

	return obj
end


function EverythingCounterCounter:Load (data)

	self.label = data.label
	self.labelColor = data.labelColor
	self.counter = data.counter
	self.counts = data.counts
	self.channel = data.channel
	self.isNotEnabled = data.isNotEnabled

end


function EverythingCounterCounter:Edit (onOkCallback)
	EverythingCounterCounter.CounterDialogOpen (self, self.id, function (old, new)
		old:Load (new)
		if (onOkCallback) then
			onOkCallback (self)
		end
	end)
end


function EverythingCounterCounter:Remove ()
end

----------------------------------------------------------------- UI: Click casting dialog
local counter_dlg = {
	isInitialized = false,
	colors = {}
}

function EverythingCounterCounter.CounterDialogIsOpen ()
	return WindowGetShowing ("EverythingCounterCounter")
end

function  EverythingCounterCounter.CounterDialogHide ()
	WindowSetShowing ("EverythingCounterCounter", false)
end

function EverythingCounterCounter.CounterDialogOpen (data, ignoreId, onOkCallback)

	counter_dlg.isLoading = true

	if (not counter_dlg.isInitialized) then
		-- initialize dialog UI
		CreateWindow ("EverythingCounterCounter", false)

		LabelSetText ("EverythingCounterCounterTitleBarText", L"Counter config")
		ButtonSetText ("EverythingCounterCounterOkButton", L"OK")
		ButtonSetText ("EverythingCounterCounterCancelButton", L"Cancel")

		LabelSetText ("EverythingCounterCounterContentScrollChildLabelLabel", L"Label:")
		LabelSetText ("EverythingCounterCounterContentScrollChildLabelColor", L"Label color:")
		local i = 1
		for k,v in pairs(everythingCounter.Colors) do
			ComboBoxAddMenuItem ("EverythingCounterCounterContentScrollChildLabelColorCombobox", towstring(k))
			counter_dlg.colors[i] = k
			i = i + 1
		end

		LabelSetText ("EverythingCounterCounterContentScrollChildCounterLabel", L"Counter:")

		LabelSetText ("EverythingCounterCounterContentScrollChildCountsLabel", L"Counts:")

		LabelSetText ("EverythingCounterCounterContentScrollChildChannelLabel", L"Channel:")

		LabelSetText ("EverythingCounterCounterContentScrollChildHelpLabel",
			L"IMPORTANT: Make sure the necessary chat filters in game are active. It can only be counted what is visible in chat!\n\n" ..

			L"Counter:\n" ..
			L"Replace numbers that should be extracted and summed up with ECNUMBER.\n" .. 
			L"Example: \"You receive a War Crest (5)\" should look like: \"You receive a War Crest (ECNUMBER)\"\n" .. 
			L"This will extracts the value 5 and add it to the current counter value.\n\n" ..

			L"Replace text or numbers you dont want to count with ECANYTHING.\n" ..
			L"Example: \"You have slain Foolz!\" should look like: \"You have slain ECANYTHING!\"\n\n" .. 

			L"Channel:\n" ..
			L"Three channles exist. \"Combat\", \"Chat\" and \"System\".\n" .. 
			L"By default all channels are monitored for a Counter. This can lead to problems when the same message appears in multiple channels at the same time, which makes the counter go up multiple times (Level up for example is shown in Chat and System.\n" .. 
			L"To avoid this you can limit a counter to specific channels. Add them seperated by a , to choose specific channels."
		) 

		counter_dlg.isInitialized = true
	end

	-- proceed parameters
	counter_dlg.oldData = data
	counter_dlg.onOkCallback = onOkCallback

	counter_dlg.data = EverythingCounterCounter.New (data)

	-- fill form with existing data if available
	TextEditBoxSetText ("EverythingCounterCounterContentScrollChildLabelText",towstring( everythingCounter.isNil( counter_dlg.data.label, L"")))
	if type(counter_dlg.data.labelColor) ~= "number" then
		counter_dlg.data.labelColor = everythingCounter.indexOf(counter_dlg.colors, counter_dlg.data.labelColor)
	end
	ComboBoxSetSelectedMenuItem ("EverythingCounterCounterContentScrollChildLabelColorCombobox", counter_dlg.data.labelColor)

	TextEditBoxSetText ("EverythingCounterCounterContentScrollChildCounterText",towstring( everythingCounter.isNil( counter_dlg.data.counter, L"")))
	TextEditBoxSetText ("EverythingCounterCounterContentScrollChildCountsText",towstring( everythingCounter.isNil( counter_dlg.data.counts, L"")))
	TextEditBoxSetText ("EverythingCounterCounterContentScrollChildChannelText",towstring( everythingCounter.isNil( counter_dlg.data.channel, L"")))

	counter_dlg.isLoading = false
	WindowSetShowing ("EverythingCounterCounter", true)

	ScrollWindowSetOffset ("EverythingCounterCounterContent", 0)
	ScrollWindowUpdateScrollRect ("EverythingCounterCounterContent")
end

function EverythingCounterCounter.OnOk ()

	if (counter_dlg.isLoading or not EverythingCounterCounter.CounterDialogIsOpen()) then return end

	counter_dlg.data.label = everythingCounter.isEmpty (TextEditBoxGetText ("EverythingCounterCounterContentScrollChildLabelText"), nil)
	counter_dlg.data.label = tostring( counter_dlg.data.label )
	counter_dlg.data.labelColor = ComboBoxGetSelectedMenuItem ("EverythingCounterCounterContentScrollChildLabelColorCombobox")
	counter_dlg.data.labelColor = counter_dlg.colors[counter_dlg.data.labelColor]
	counter_dlg.data.labelColor = tostring( counter_dlg.data.labelColor )
	counter_dlg.data.counter = everythingCounter.isEmpty (TextEditBoxGetText ("EverythingCounterCounterContentScrollChildCounterText"), nil)
	counter_dlg.data.counter = tostring( counter_dlg.data.counter )
	counter_dlg.data.counts = everythingCounter.isEmpty (TextEditBoxGetText ("EverythingCounterCounterContentScrollChildCountsText"), nil)
	counter_dlg.data.counts = tonumber( counter_dlg.data.counts )
	counter_dlg.data.channel = everythingCounter.isEmpty (TextEditBoxGetText ("EverythingCounterCounterContentScrollChildChannelText"), nil)
	counter_dlg.data.channel = tostring( counter_dlg.data.channel )

	if (not counter_dlg.data.label) then
		DialogManager.MakeOneButtonDialog (L"You must enter a label.", L"Ok")
		return
	end

	if (counter_dlg.onOkCallback) then
		counter_dlg.onOkCallback (counter_dlg.oldData, counter_dlg.data)
	end

	EverythingCounterCounter.CounterDialogHide()
end