if not everythingCounter then everythingCounter = {} end

-- TODO: Add chanel to each counter to avoid double coutns

local towstring = towstring
local CreateHyperLink = CreateHyperLink
local DoesWindowExist = DoesWindowExist
local table_insert = table.insert;
local table_remove = table.remove;
local table_sort = table.sort;
local table_getn = table.getn;
local string_format	= string.format;
local string_gsub 	= string.gsub
local string_format	= string.format
local string_gmatch	= string.gmatch
local everythingCounterWindowName = "everythingCounterWindow";
local inLoadingScreen = false;
local debugMode = false;
local eventCache = {};

everythingCounter.activeSettings = {};

local function Print(str)
	EA_ChatWindow.Print(towstring(str));
end

function everythingCounter.OnInitialize()

	-- load persistent settings
	if not everythingCounter.Settings then 
		everythingCounter.Settings = everythingCounter.DefaultSettings;
	end
	everythingCounter.activeSettings = everythingCounter.Settings

	if (DoesWindowExist(everythingCounterWindowName) == false) then
		everythingCounter.createEverythingCounterWindow();
	end
	
	RegisterEventHandler( TextLogGetUpdateEventId( "Chat" ), "everythingCounter.OnChat" )
	RegisterEventHandler( TextLogGetUpdateEventId( "System" ), "everythingCounter.OnSystem" )
	RegisterEventHandler( TextLogGetUpdateEventId( "Combat" ), "everythingCounter.OnCombat" )

	RegisterEventHandler( SystemData.Events.LOADING_BEGIN, "everythingCounter.LOADING_START");
	RegisterEventHandler( SystemData.Events.LOADING_END, "everythingCounter.LOADING_END");
	
	if LibSlash then
		-- register LibSlash command "/everythingCounter" and tries "/wlh"
		LibSlash.RegisterSlashCmd("everythingCounter", function(args) everythingCounter.SlashCmd(args) end);
		if (not LibSlash.IsSlashCmdRegistered("evc")) then
			LibSlash.RegisterSlashCmd("evc", function(args) everythingCounter.SlashCmd(args) end);
		end
		Print("<LINK data=\"0\" text=\"[everythingCounter]\" color=\"50,255,10\"> Addon initialized. Use /everythingCounter to show options.");
	else
		Print("<LINK data=\"0\" text=\"[everythingCounter]\" color=\"50,255,10\"> Addon initialized.");
	end
	
end

function everythingCounter.OnShutdown()
	chatHookActive = false
end

function everythingCounter.LOADING_START()
	inLoadingScreen = true;
end

function everythingCounter.LOADING_END()
	inLoadingScreen = false;
end


function everythingCounter.SlashCmd(args)

	local command;
	local parameter;
	local separator = string.find(args," ");
	
	if separator then
		command = string.sub(args, 0, separator - 1);
		parameter = string.sub(args, separator + 1, -1);
	else
		command = args;
	end

	if ( command == "show"or command == "s" or command == "toggle" or command == "t") then
		everythingCounter.toggleEverythingCounterWindow();
	elseif ( command == "config" or command == "cfg" or command == "c") then
		everythingCounterConfigWindow.Show();
	else 
		Print("<LINK data=\"0\" text=\"[everythingCounter]\" color=\"50,255,10\"> /everythingCounter show - toggle the main window.");
		Print("<LINK data=\"0\" text=\"[everythingCounter]\" color=\"50,255,10\"> /everythingCounter config - open the configuration window.");
	end
	
end

function everythingCounter.createEverythingCounterWindow()

	if (DoesWindowExist(everythingCounterWindowName) == false) then 
		CreateWindow(everythingCounterWindowName, false);
	end

	WindowSetDimensions( everythingCounterWindowName, everythingCounter.activeSettings.windowCounterWidth + 26, 26);

	-- title label background
	WindowSetTintColor(everythingCounterWindowName .. "TitleBackground", 0, 0, 0);
	WindowSetAlpha(everythingCounterWindowName .. "TitleBackground", 0.15);

	WindowSetDimensions( everythingCounterWindowName .. "Title", everythingCounter.activeSettings.windowCounterWidth, 26);
	WindowSetDimensions( everythingCounterWindowName .. "TitleLabel", everythingCounter.activeSettings.windowCounterWidth, 26);

	ButtonSetText (everythingCounterWindowName .."CloseButton", L"X");
	ButtonSetText (everythingCounterWindowName .."ConfigButton", L"C");

	WindowSetScale( everythingCounterWindowName, everythingCounter.activeSettings.windowSize );

	everythingCounter.drawWindows(everythingCounter.activeSettings.counter);
end

local timerDelay = 5;
local timeLeft = 1;
local timers = {};
function everythingCounter.timerUpdate(elapsed)

	if (inLoadingScreen == true) then return end

	timeLeft = timeLeft - elapsed;
    if (timeLeft > 0) then
        return;
    end
	
	if( DoesWindowExist(everythingCounterConfigWindow.name) and WindowGetShowing(everythingCounterConfigWindow.name) ) then
		debugMode = true;
    else
		debugMode = false;
    end

	timeLeft = timerDelay;
end

function everythingCounter.toggleEverythingCounterWindow()
	everythingCounter.drawWindows(everythingCounter.activeSettings.counter, true);
end

function everythingCounter.drawWindows(items, toggle)

	if ( not items ) then return end

	local isVisible = WindowGetShowing(everythingCounterWindowName);
	if (toggle) then
		WindowSetShowing(everythingCounterWindowName, not isVisible);
	end

	everythingCounter.destroyWindows(100);

	-- draw windows for all messages
	local bottomWindow = "everythingCounterWindowTitle";
	for i=1, #items do

		if ( not items[i].isNotEnabled) then

			local label = items[i].label or items[i];
			local counts = items[i].counts or 0;
			local color = everythingCounter.Colors[items[i].labelColor] or everythingCounter.Colors["white"];

			local counterWindow = "everythingCounter_Counter_" .. tostring(i);	
			if (DoesWindowExist(counterWindow) == false) then
				CreateWindowFromTemplate( counterWindow, "everythingCounter_Counter_Template", "Root");
			end
			
			WindowSetShowing( counterWindow, isVisible);
			WindowSetTintColor(counterWindow .. "Background", 0, 0, 0);
			WindowSetAlpha(counterWindow .. "Background", 0.15);

			LabelSetFont( counterWindow .. "Label", "font_clear_small_bold", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
			LabelSetText( counterWindow .. "Label", towstring(label));
			LabelSetTextColor( counterWindow .. "Label", color[1], color[2], color[3])
			WindowSetDimensions( counterWindow .. "Label", everythingCounter.activeSettings.windowCounterWidth, everythingCounter.activeSettings.windowCounterHeight);

			LabelSetFont( counterWindow .. "Label2", "font_clear_small_bold", WindowUtils.FONT_DEFAULT_TEXT_LINESPACING)
			LabelSetText( counterWindow .. "Label2", towstring(counts));
			LabelSetTextColor( counterWindow .. "Label2", color[1], color[2], color[3])
			WindowSetDimensions( counterWindow .. "Label2", everythingCounter.activeSettings.windowCounterWidth, everythingCounter.activeSettings.windowCounterHeight);

			WindowClearAnchors( counterWindow );
			WindowSetDimensions( counterWindow, everythingCounter.activeSettings.windowCounterWidth, everythingCounter.activeSettings.windowCounterHeight);
			WindowSetScale( counterWindow, everythingCounter.activeSettings.windowSize );

			WindowAddAnchor( counterWindow , "bottomleft", bottomWindow, "topleft", 0, 0 );
					
			if (toggle) then
				WindowSetShowing( counterWindow, not isVisible);
			else
				WindowSetShowing( counterWindow, isVisible);
			end

			bottomWindow = counterWindow;

		end
	end
end

function everythingCounter.destroyWindows(number)

	if ( not number ) then return end

	for i=1, number do
		local counterWindow = "everythingCounter_Counter_" .. tostring(i);	
		if (DoesWindowExist(counterWindow) == true) then
			DestroyWindow(counterWindow);
		end
	end	
end

function everythingCounter.OnChat(updateType, filterType)
    if updateType ~= SystemData.TextLogUpdate.ADDED then return end

    everythingCounter.OnEvent(updateType, filterType, "Chat")
end

function everythingCounter.OnSystem(updateType, filterType)
    if updateType ~= SystemData.TextLogUpdate.ADDED then return end

    everythingCounter.OnEvent(updateType, filterType, "System")

	--filterType ~= SystemData.ChatLogFilters.RVR_KILLS_ORDER
end

function everythingCounter.OnCombat(updateType, filterType)
    if updateType ~= SystemData.TextLogUpdate.ADDED then return end

    everythingCounter.OnEvent(updateType, filterType, "Combat")
end

function everythingCounter.OnEvent(updateType, filterType, channel)
    if updateType ~= SystemData.TextLogUpdate.ADDED then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_1 then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_2 then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_3 then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_4 then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_5 then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_6 then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_7 then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_8 then return end
    if filterType == SystemData.ChatLogFilters.CHANNEL_9 then return end
    if filterType == SystemData.ChatLogFilters.GROUP then return end
    if filterType == SystemData.ChatLogFilters.ALLIANCE then return end
    if filterType == SystemData.ChatLogFilters.REALM_WAR_T1 then return end
    if filterType == SystemData.ChatLogFilters.REALM_WAR_T2 then return end
    if filterType == SystemData.ChatLogFilters.REALM_WAR_T3 then return end
    if filterType == SystemData.ChatLogFilters.REALM_WAR_T4 then return end
    if filterType == SystemData.ChatLogFilters.SHOUT then return end
    if filterType == SystemData.ChatLogFilters.TRADE then return end
    if filterType == SystemData.ChatLogFilters.TELL_RECEIVE then return end
    if filterType == SystemData.ChatLogFilters.TELL_SEND then return end
    if filterType == SystemData.ChatLogFilters.SCENARIO_GROUPS then return end
    if filterType == SystemData.ChatLogFilters.SCENARIO then return end
    if filterType == SystemData.ChatLogFilters.ADVICE then return end

	--[[CITY_ANNOUNCE = 21
	KILLS_DEATH_OTHER = 23
	YOUR_HITS = 1002
	MONSTER_SAY = 7
	RVR_KILLS_ORDER = 19
	CHANNEL_1 = 57
	GROUP = 4
	USER_ERROR = 26
	ZONE_AREA = 24
	CHANNEL_5 = 61
	CHANNEL_2 = 58
	GUILD_OFFICER = 9
	DEBUG = 18
	CRAFTING = 34
	SAY = 0
	PET_DMG = 1004
	EXP = 1007
	CHANNEL_8 = 64
	YOUR_DMG_FROM_PC = 1001
	CHANNEL_7 = 63
	ALLIANCE = 12
	CHANNEL_6 = 62
	RVR_KILLS_DESTRUCTION = 20
	QUEST = 25
	INFL = 1009
	OTHER_HITS = 1006
	KILLS_DEATH_YOURS = 22
	YOUR_HEALS = 1003
	REALM_WAR_T1 = 36
	SHOUT = 11
	EMOTE = 10
	RVR = 17
	ABILITY_ERROR = 1011
	COMBAT_DEFAULT = 1000
	TRADE = 32
	CHANNEL_9 = 65
	BATTLEGROUP = 14
	TELL_RECEIVE = 1
	YOUR_DMG_FROM_NPC = 1012
	ALLIANCE_OFFICER = 13
	REALM_WAR_T2 = 37
	CHANNEL_3 = 59
	MONSTER_EMOTE = 29
	MISC = 27
	LOOT_ROLL = 31
	LOOT_COIN = 33
	CHANNEL_4 = 60
	LOOT = 30
	SCENARIO_GROUPS = 16
	TELL_SEND = 2
	REALM_WAR_T4 = 39
	TOK = 1010
	REALM_WAR_T3 = 38
	ADVICE = 35
	PET_HITS = 1005
	GUILD = 8
	SCENARIO = 15
	RENOWN = 1008--]]

    local _, filterId, text = TextLogGetEntry(channel, TextLogGetNumEntries(channel) - 1);
    if debugMode then
    	local str = "DEBUG: " .. tostring(channel) .. " | filterType: " .. tostring(filterType) .. " | " .. tostring(text);
    	d(str)
	end

    local dateTimePack = everythingCounter.DateTime_GetCurrentDateTimePack();

    local counterPattern
    local numAdd = 1
    local foundNum
    local item
    local itemChannel
    for i=1, #everythingCounter.activeSettings.counter do
    	item = everythingCounter.activeSettings.counter[i];
		if not item.isNotEnabled then
	    	itemChannel = item.channel;
			if not itemChannel or itemChannel == "" then itemChannel = "Chat, Combat, System" end
    		itemChannel = string.lower(itemChannel);

			if itemChannel:find(string.lower(channel)) then
		    	counterPattern = item.counter;
		    	counterPattern = everythingCounter.escapePattern(counterPattern);
		    	counterPattern = counterPattern:gsub("ECNUMBER", "(%%%d+)");
		    	counterPattern = counterPattern:gsub("ECANYTHING", ".+");
		    	
			    if text:match(towstring(counterPattern)) then
			    	--if no number was captured it's a pattern without numbers and counts as 1
			    	foundNum = tonumber(text:match(towstring(counterPattern)));
			    	if foundNum > 0 then
			    		numAdd = foundNum;
			    		foundNum = 0;
			    	end
			    	if item.counts == 0 then
			   			everythingCounter.activeSettings.counter[i].startDate = dateTimePack;
			    	end
		   			everythingCounter.activeSettings.counter[i].lastDate = dateTimePack;
			   		everythingCounter.activeSettings.counter[i].counts = item.counts + tonumber(numAdd);
			   		everythingCounter.Settings.counter[i] = everythingCounter.activeSettings.counter[i];
			   		everythingCounter.drawWindows(everythingCounter.activeSettings.counter);
			    end
			end
		end
    end
end

function everythingCounter.OnMouseOverCreateTooltip()
	local selectedWindow = towstring(SystemData.ActiveWindow.name);
	local mouseoverMessageId = selectedWindow:match(L"everythingCounter_Counter_(%d+)Background");
	mouseoverMessageId = tonumber(mouseoverMessageId);
	local item = everythingCounter.activeSettings.counter[mouseoverMessageId];
	local startDate = everythingCounter.DateTime_UnpackDateTime(item.startDate) or "-"

	if startDate == "-" then return end

	if startDate ~= "-" then
		startDate = startDate.year .. "-" .. startDate.month .. "-" .. startDate.day .. " " .. startDate.hour .. ":" .. startDate.minute .. ":" .. startDate.second
	end
	local lastDate = everythingCounter.DateTime_UnpackDateTime(item.lastDate) or "-"
	if lastDate ~= "-" then
		lastDate = lastDate.year .. "-" .. lastDate.month .. "-" .. lastDate.day .. " " .. lastDate.hour .. ":" .. lastDate.minute .. ":" .. lastDate.second
	end

	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name )
    Tooltips.SetTooltipText( 1, 1, towstring(item.label .. " " .. item.counts .. "\nFirst time: " ..  startDate .. "\nLast time:  " .. lastDate))
    Tooltips.AnchorTooltip (Tooltips.ANCHOR_CURSOR)
    Tooltips.Finalize();
end

function everythingCounter.escapePattern(text)
    return text:gsub("([^%w])", "%%%1")
end


function everythingCounter.trim(s)
   return (s:gsub("^%s*(.-)%s*$", "%1"))
end

function everythingCounter.setActiveSettings(settings)
	activeSettings = settings;
end 

function everythingCounter.isNil (value, nilReturnValue)
	if (value == nil) then return nilReturnValue end
	return value
end

function everythingCounter.isEmpty (value, emptyReturnValue)
	if (not value or value:len() < 1) then return emptyReturnValue end
	return value
end

function everythingCounter.indexOf(array, value)
    for i, v in ipairs(array) do
        if v == value then
            return i
        end
    end
    return nil
end

function everythingCounter.DateTime_GetCurrentDate()
	local a = {}
	
	local today = GetTodaysDate()
	
	a.day 	= today.todaysDay
	a.month = today.todaysMonth
	a.year 	= today.todaysYear
	
	return a
end

function everythingCounter.DateTime_GetCurrentDateTime()
	local time = everythingCounter.DateTime_GetCurrentTime()
	local date = everythingCounter.DateTime_GetCurrentDate()
	
	for k, v in pairs( date )
	do
		time[k] = v	
	end
	
	return time
end

function everythingCounter.DateTime_GetCurrentDateTimePack()
	return everythingCounter.DateTime_PackDateTime( everythingCounter.DateTime_GetCurrentDateTime() )
end

function everythingCounter.DateTime_GetCurrentTime()
	local a = {}
	
	local time = GetComputerTime()
	
	a.second 	= time % 60
	a.minute 	= ( ( time - a.second ) / 60 ) % 60
	a.hour 		= ( ( ( ( time - a.second ) / 60 - a.minute ) / 60 ) % 60 )
	
	return a
end

function everythingCounter.DateTime_PackDateTime( dt )
	return string_format( "%s:%s:%s:%s:%s:%s", dt.year, dt.month, dt.day, dt.hour, dt.minute, dt.second )
end

function everythingCounter.DateTime_UnpackDateTime( dt )

	if not dt then return end 

	local data, a = {}, {}
	
	data = everythingCounter.strsplit( dt )
	
	a.year 		= data[1]
	a.month 	= data[2]
	a.day 		= data[3]
	a.hour 		= data[4]
	a.minute 	= data[5]
	a.second 	= data[6]
	
	return a
end

function everythingCounter.strsplit( str, sep )
    local sep, fields = sep or ":", {}
    local pattern = string_format( "([^%s]+)", sep )
    string_gsub( str, pattern, function(c) fields[#fields+1] = c end )
    return fields
end