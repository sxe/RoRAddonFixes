Counts reccuring chat events, to keep track of kills, loot and everything else you can see in chat. Run /everythingCounter command for instructions.

Functionality:

	* type /everythingCounter show to open the counter window

	* type /everythingCounter config to open the configuration window

----------------

Installation:

	* Get it from here: https://tools.idrinth.de/addons/everythingCounter/

-----------------


0.1 - Updated as of 5/06/2022.
* Initial Release