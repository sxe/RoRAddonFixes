if not everythingCounter then everythingCounter = {} end

-- You can get the numbers for the colors here: https://www.rapidtables.com/web/color/RGB_Color.html
everythingCounter.Colors = {
	white = {255, 255, 255},
    red = {240, 60, 60},
    green = {0, 150, 50},
    blue = {60, 120, 200},
    yellow = {255, 170, 70},
    grey = {150, 150, 150},
    black = {0, 0, 0}    
}

everythingCounter.DefaultSettings = {
	windowSize = 0.7, -- scales the size of the window (doh..) 0.5 is 50% for example
	windowCounterWidth = 130,
	windowCounterHeight = 18,
	counter = {
		{ label = "War Crests", labelColor = "white", counter = "You receive a War Crest (ECNUMBER)", counts = 0, isNotEnabled = false },		
		{ label = "Kills", labelColor = "green", counter = "You have slain ECANYTHING!", counts = 0, isNotEnabled = false },
		{ label = "Deaths", labelColor = "red", counter = "You have been slain by ECANYTHING!", counts = 0, isNotEnabled = false },
		{ label = "Level Up", labelColor = "yellow", counter = "Your rank has increased to ECANYTHING!", counts = 0, channel = "Chat", isNotEnabled = true },
		{ label = "Renown Up", labelColor = "blue", counter = "Your renown rank has increased to ECANYTHING!", counts = 0, channel = "Chat", isNotEnabled = true },
		{ label = "Quests", labelColor = "yellow", counter = "ECANYTHING completed!", counts = 0, channel = "Chat", isNotEnabled = true },
		{ label = "Test", labelColor = "white", counter = "Test Pattern ECANYTHING!", counts = 0, channel = "Chat, System", isNotEnabled = true },
	},
}
