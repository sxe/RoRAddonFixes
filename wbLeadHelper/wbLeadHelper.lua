if not wbLeadHelper then wbLeadHelper = {} end

-- TODO: maybe add a moral build + dump button
-- TODO: Shortcuts for buttons, maybe

local activeTemplate = wbLeadHelper.Templates[1];
local towstring = towstring
local CreateHyperLink = CreateHyperLink
local DoesWindowExist = DoesWindowExist
local table_insert = table.insert;
local table_sort = table.sort;
local wbLeadHelperWindowName = "wbLeadHelperWindow";
local showingAlternateTitle = false;
local inLoadingScreen = false;
local timerActive = 0;
local activeChatChannel = "";
local subMenuActive = false;
local zoneMenuActive = false;
local boMenuActive = false;
local lfmMenuActive = false;
local countdownMenuActive = false;
local clickedMainMenuMessageId = 0;
local tmpMessageActive = "";
local tmpColorActive = "";
local hookOnKeyEnter;
local chatHookActive = false;

wbLeadHelper.activeSettings = {};

local function Print(str)
	EA_ChatWindow.Print(towstring(str));
end

function wbLeadHelper.OnInitialize()

	-- load persistent settings
	if not wbLeadHelper.Settings then 
		wbLeadHelper.Settings = wbLeadHelper.DefaultSettings;
	end
	wbLeadHelper.setActiveSettings(wbLeadHelper.Settings)

	--check if settinsg have to be reset
	if not wbLeadHelper.Settings.settingsVersion or wbLeadHelper.Settings.settingsVersion < wbLeadHelper.DefaultSettings.settingsVersion then
		wbLeadHelper.DefaultSettingsChangedDialog();
	end

	hookOnKeyEnter = EA_ChatWindow.OnKeyEnter;
	EA_ChatWindow.OnKeyEnter = wbLeadHelper.OnKeyEnter;
	chatHookActive = false

	if (DoesWindowExist(wbLeadHelperWindowName) == false) then
		wbLeadHelper.createWbLeadHelperWindow();
	end
	
	RegisterEventHandler( SystemData.Events.LOADING_BEGIN, "wbLeadHelper.LOADING_START");
	RegisterEventHandler( SystemData.Events.LOADING_END, "wbLeadHelper.LOADING_END");
	
	if LibSlash then
		-- register LibSlash command "/wbLeadHelper" and tries "/wlh"
		LibSlash.RegisterSlashCmd("wbLeadHelper", function(args) wbLeadHelper.SlashCmd(args) end);
		if (not LibSlash.IsSlashCmdRegistered("wlh")) then
			LibSlash.RegisterSlashCmd("wlh", function(args) wbLeadHelper.SlashCmd(args) end);
		end
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> Addon initialized. Use /wlh or /wbLeadHelper to show options.");
	else
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> Addon initialized.");
	end
	
end

function wbLeadHelper.OnShutdown()
	EA_ChatWindow.OnKeyEnter = hookOnKeyEnter;
	chatHookActive = false
end

function wbLeadHelper.LOADING_START()
	inLoadingScreen = true;
end

function wbLeadHelper.LOADING_END()
	inLoadingScreen = false;
end


function wbLeadHelper.SlashCmd(args)

	local command;
	local parameter;
	local separator = string.find(args," ");
	
	if separator then
		command = string.sub(args, 0, separator - 1);
		parameter = string.sub(args, separator + 1, -1);
	else
		command = args;
	end
	

	if ( command == "show"or command == "s" or command == "toggle" or command == "t") then
		wbLeadHelper.toggleWbLeadHelperWindow();
	elseif ( command == "config" or command == "cfg" or command == "c") then
		wbLeadHelperConfigWindow.Show();
	else 
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> /wlh show - toggle the main window.");
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> /wlh config - open the configuration window.");
	end
	
end

function wbLeadHelper.createWbLeadHelperWindow()

	if (DoesWindowExist(wbLeadHelperWindowName) == false) then 
		CreateWindow(wbLeadHelperWindowName, false);
	end
	showingAlternateTitle = false;
		
	-- title label background
	WindowSetTintColor(wbLeadHelperWindowName .. "TitleBackground", 0, 0, 0);
	WindowSetAlpha(wbLeadHelperWindowName .. "TitleBackground", 0.55);

	WindowSetDimensions( wbLeadHelperWindowName .. "Title", activeTemplate.titleWidth, 30);
	WindowSetDimensions( wbLeadHelperWindowName .. "TitleLabel", activeTemplate.titleWidth, 30);

	ButtonSetText (wbLeadHelperWindowName .."CloseButton", L"X");
	ButtonSetText (wbLeadHelperWindowName .."ChatButton", L"CM");

	WindowSetScale( wbLeadHelperWindowName, wbLeadHelper.activeSettings.windowSize );

	wbLeadHelper.draw();
	
end

function wbLeadHelper.hideWbLeadHelperWindowName()
	wbLeadHelper.toggleWbLeadHelperWindow();
end

local TIME_DELAY2 = 1;
local timeLeft2 = 1;
local timerDelay = 5;
function wbLeadHelper.timerUpdate(elapsed)

	if (inLoadingScreen == true) then return end

	timeLeft2 = timeLeft2 - elapsed;
    if (timeLeft2 > 0) then
        return;
    end
	
	if (timerActive > 10) then
		if (timerDelay == 1 ) then
			--d(timerActive);
			--d(activeChatChannel);
			wbLeadHelper.chat(timerActive, activeChatChannel, tmpColorActive);
			timerDelay = 5;
		else
			timerDelay = timerDelay -1;
		end
		--d("delay: " .. tostring(timerDelay));
	elseif (timerActive > 0) then
		--d(timerActive);
		--d(activeChatChannel);
		wbLeadHelper.chat(timerActive, activeChatChannel, tmpColorActive);
		timerDelay = 5;
	end
	if (timerActive > 0) then
		timerActive = timerActive -1;
	end
	
	timeLeft2 = TIME_DELAY2;
end

function wbLeadHelper.draw(toggle)

	if ( zoneMenuActive == true) then return end
	if ( boMenuActive == true) then return end
	if ( lfmMenuActive == true) then return end
	if ( countdownMenuActive == true) then return end
	
	wbLeadHelper.drawWindows(wbLeadHelper.activeSettings.messages, toggle);

end

function wbLeadHelper.drawCountdownMenu(countdowns)
	wbLeadHelper.drawWindows(countdowns);
end

function wbLeadHelper.drawZoneMenu(zoneNames)
	wbLeadHelper.drawWindows(zoneNames);
end

function wbLeadHelper.drawBoMenu(bos)
	wbLeadHelper.drawWindows(bos);
end

function wbLeadHelper.drawLfmMenu(lfm)
	wbLeadHelper.drawWindows(lfm);
end

function wbLeadHelper.drawWindows(items, toggle)

	if ( not items ) then return end

	local isVisible = WindowGetShowing(wbLeadHelperWindowName);
	if (toggle) then
		WindowSetShowing(wbLeadHelperWindowName, not isVisible);
	end

	wbLeadHelper.destroyWindows(100);

	-- draw windows for all messages
	local xOffset = 0;
	local yOffset = (3 * wbLeadHelper.activeSettings.windowSize);
	local bottomWindow = "wbLeadHelperWindowTitle";
	local j = 1;
	for i=1, #items do
		repeat

			if type(items[i]) == "table" and not items[i].submenu then 
				items[i].submenu = "None";
			end

			if items[i].isNotEnabled then
				do break end
			end

			if not subMenuActive and items[i].submenu ~= "None" then 
				do break end
			end 

			local label = items[i].label or items[i];
			local color = wbLeadHelper.Colors[items[i].labelColor] or wbLeadHelper.Colors["white"];

			-- replace linebreaks for text formatting
			label = label:gsub("<br>", "\n", 1); --for backwards compatibility
			label = label:gsub("<nl>", "\n", 1);

			-- remove remaining tags so they are not shown in the label
			label = label:gsub("<", "");
			label = label:gsub(">", "");

			local messageWindow = "wbLeadHelper_Message_" .. tostring(i);	
			if (DoesWindowExist(messageWindow) == false) then
				CreateWindowFromTemplate( messageWindow, "wbLeadHelper_Message_Template", "Root");
			end
			
			WindowSetShowing( messageWindow, isVisible);
			WindowSetTintColor(messageWindow .. "Background", 0, 0, 0);
			WindowSetAlpha(messageWindow .. "Background", 0.5);

			LabelSetText( messageWindow .. "Label", towstring(label));
			LabelSetTextColor(messageWindow .. "Label", color[1], color[2], color[3])

			WindowSetDimensions( messageWindow .. "Label", activeTemplate.buttonWidth, activeTemplate.buttonHeight);
			WindowClearAnchors( messageWindow );
			WindowSetDimensions( messageWindow, activeTemplate.buttonWidth, activeTemplate.buttonHeight);

			WindowAddAnchor( messageWindow , "bottomleft", bottomWindow, "topleft", (xOffset / InterfaceCore.GetScale()) * wbLeadHelper.activeSettings.windowSize, (2 / InterfaceCore.GetScale()) * wbLeadHelper.activeSettings.windowSize );
			WindowSetScale( messageWindow, wbLeadHelper.activeSettings.windowSize );
					
			if (toggle) then
				WindowSetShowing( messageWindow, not isVisible);
			else
				WindowSetShowing( messageWindow, isVisible);
			end

			bottomWindow = messageWindow;

			if (j % activeTemplate.buttonsPerRow == 0) then
				xOffset = xOffset + (activeTemplate.buttonWidth + 2) * (j / activeTemplate.buttonsPerRow);
				bottomWindow = "wbLeadHelperWindowTitle";
			else 
				xOffset = 0;
			end

			j = j +1 ;

		until true
	end

	if (showingAlternateTitle == false) then
		wbLeadHelper.showNormalTitle();
	end
end

function wbLeadHelper.extractColor(message)

	if ( not message ) then return end

	local color = wbLeadHelper.Colors["white"];
	local aColor, cColor;
	if(message:match("<%w+>")) then
		aColor = message:match("<(%w+)>");
		cColor = wbLeadHelper.Colors[tostring(aColor)];
		if(cColor) then
			message = message:gsub("<%w+>", "", 1);
			message = wbLeadHelper.trim(message);
			return message, cColor
		end
	end

	return message, color
end

function wbLeadHelper.destroyWindows(number)

	if ( not number ) then return end

	for i=1, number do
		local messageWindow = "wbLeadHelper_Message_" .. tostring(i);	
		if (DoesWindowExist(messageWindow) == true) then
			DestroyWindow(messageWindow);
		end
	end	
end

function wbLeadHelper.toggleWbLeadHelperWindow()
	wbLeadHelper.draw(true);	
end

function wbLeadHelper.onLMB()
	wbLeadHelper.toChat("lmb");
end

function wbLeadHelper.onRMB()
	wbLeadHelper.toChat("rmb");
end

function wbLeadHelper.toChat(mouseButton)

	local selectedWindow = towstring(SystemData.ActiveWindow.name);
	local mouseoverMessageId = selectedWindow:match(L"wbLeadHelper_Message_(%d+)");
	mouseoverMessageId = tonumber(mouseoverMessageId);

	local item = wbLeadHelper.activeSettings.messages[mouseoverMessageId];
	local message = "";
	if ( zoneMenuActive ) then
		local zoneNames = wbLeadHelper.getAllZoneNames();
		message = zoneNames[mouseoverMessageId];
	elseif ( boMenuActive ) then
		local bos = wbLeadHelper.getBObyZoneId(wbLeadHelper.getCurrentZone());
		message = bos[mouseoverMessageId];
	elseif ( lfmMenuActive ) then
		local lfm = wbLeadHelper.getSubmenuMessages();
		message = lfm[mouseoverMessageId];
	elseif ( countdownMenuActive ) then
		local countdowns = wbLeadHelper.getCountDowns();
		message = countdowns[mouseoverMessageId];
	else
		message = item.message;
	end

	-- extract chat channel from message
	local chat = wbLeadHelper.setChatChannel(mouseButton, message);
	message = message:gsub("^/%w+ ", "");	

	-- extract message color from message
	local color = wbLeadHelper.Colors[item.messageColor] or wbLeadHelper.Colors["white"];

	-- replace group leader tag
	if (message:match("<GL>")) then
		local groupLeader = "me";
		local checkLeader = wbLeadHelper.getWbLeader();
		if( checkLeader ~= nil and checkLeader ~= '') then 
			groupLeader = "@" .. checkLeader;
		end
		message = message:gsub("<GL>", groupLeader);
	end


	--
	--
	-- When a sub menu is active or no more menus follow
	--
	--
	if(countdownMenuActive) then
		-- countdown menu is active
		wbLeadHelper.destroyWindows(100);

		if(message:match("- BACK -")) then
		else
			local timerVal = tonumber(message);
			timerActive = timerVal -1; -- this value is monitored by the game update function.
			activeChatChannel = chat;
			wbLeadHelper.chat( timerVal .. " sec countdown started!", chat, tmpColorActive); --only the initial value is put in chat here
		end
		countdownMenuActive = false;
		subMenuActive = false;
		wbLeadHelper.draw(); -- draw here just to avoid a delay in the messages showing up again

	-- zoneMenu is active 
	elseif (zoneMenuActive) then

		wbLeadHelper.destroyWindows(100);

		if(message:match("- BACK -")) then
		else
			local newMessage = message:upper();
			newMessage = tmpMessageActive:gsub("<Z>", newMessage);
			wbLeadHelper.chat(newMessage, chat, tmpColorActive);
		end
		zoneMenuActive = false;
		subMenuActive = false;
		tmpMessageActive = "";
		tmpColorActive = "";

		wbLeadHelper.draw(); -- draw here just to avoid a delay in the messages showing up again

	-- boMenu is active 
	elseif (boMenuActive) then

		wbLeadHelper.destroyWindows(100);

		if(message:match("- BACK -")) then
		else
			local newMessage = message:upper();
			newMessage = tmpMessageActive:gsub("<B>", newMessage);
			wbLeadHelper.chat(newMessage, chat, tmpColorActive);
		end
		boMenuActive = false;
		subMenuActive = false;
		tmpMessageActive = "";
		tmpColorActive = "";

		wbLeadHelper.draw(); -- draw here just to avoid a delay in the messages showing up again

	elseif (lfmMenuActive) then

		wbLeadHelper.destroyWindows(100);

		if(message:match("- BACK -")) then
		else
			local newMessage = message;
			local match = message:match("<autoRoles(%d+)>");
			if (match) then
				match = tonumber(match);
				newMessage = wbLeadHelper.AutoLookingForPlayers(match);
				if newMessage == "2" then
					Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"255,50,50\"> Warband full.");
				elseif not newMessage then 
					--this path should not be reached anymore, since a group with just you is used in case you are not in a warband.
					Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"255,50,50\"> Please join a warband before using the auto search feature.");
				end		
			end

			if (newMessage and newMessage ~= "2") then
				newMessage = tmpMessageActive:gsub("<LFM>", newMessage);
				local zone = GetZoneName(wbLeadHelper.getCurrentZone());
				newMessage = newMessage:gsub("<LFMZ>", tostring(zone));

				newMessage = wbLeadHelper.addCareerIcons(newMessage);
	
				wbLeadHelper.chat(newMessage, chat, tmpColorActive);
			end
		end
		lfmMenuActive = false;
		subMenuActive = false;
		tmpMessageActive = "";
		tmpColorActive = "";

		wbLeadHelper.draw(); -- draw here just to avoid a delay in the messages showing up again


	--
	--
	-- When a first level menu is active (e.g. the initial messages are shown)
	--
	--
	elseif (item.type == "Countdown") then

		if(timerActive ~= 0) then
			timerActive = 0;
			wbLeadHelper.chat("Countdown aborted!", chat, color);
		else
			countdownMenuActive = true;
			subMenuActive = true;
			local countdowns = wbLeadHelper.getCountDowns();
			wbLeadHelper.drawCountdownMenu(countdowns);
			tmpColorActive = color;
		end

	elseif (item.type == "Zones") then

		zoneMenuActive = true;
		subMenuActive = true;
		tmpMessageActive = message;
		tmpColorActive = color;

		local zoneNames = wbLeadHelper.getAllZoneNames();

		wbLeadHelper.drawZoneMenu(zoneNames);
	elseif (item.type == "LFM") then

		lfmMenuActive = true;
		subMenuActive = true;
		tmpMessageActive = message;
		tmpColorActive = color;
		clickedMainMenuMessageId = mouseoverMessageId;

		local lfmNames = wbLeadHelper.getSubmenuMessages("label");

		wbLeadHelper.drawLfmMenu(lfmNames);

	elseif (item.type == "Bos") then

		local bos = wbLeadHelper.getBObyZoneId(wbLeadHelper.getCurrentZone());

		boMenuActive = true;
		subMenuActive = true;
		tmpMessageActive = message;
		tmpColorActive = color;

		wbLeadHelper.drawBoMenu(bos);

	else
		wbLeadHelper.chat(message, chat, color);	
	end
end

function wbLeadHelper.chat(message, channel, color)

	local message = wbLeadHelper.activeSettings.messageStart .. " " .. message .. " " .. wbLeadHelper.activeSettings.messageEnd;
	local message = wbLeadHelper.ColorText(message, color);
	message = channel .. message;

	--EA_ChatWindow.InsertText( towstring(message), towstring(channel) );
	if( WindowGetShowing(wbLeadHelperConfigWindow.name)) then
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> " .. message);
	else
		SendChatText( towstring(message), L"" );
	end
end

function wbLeadHelper.addCareerIcons(newMessage)
	local tankIcons = "<icon22702>";
	local healerIcons = "<icon22706>";
	local dpsIcons = "<icon22701>"; 

	if(wbLeadHelper.activeSettings.showLfgIcons) then
		tankIcons, healerIcons, dpsIcons = wbLeadHelper.getOwnFactionCareerIconsByType();
	end

	newMessage = newMessage:gsub("<healer>", healerIcons.." healer");
	newMessage = newMessage:gsub("<tank>", tankIcons.." tank");
	newMessage = newMessage:gsub("<dps>", dpsIcons.." dps");

	return newMessage;
end

function wbLeadHelper.setChatChannel(mb, text)

	local chat = "/1 ";

	if (mb == "lmb") then
		chat = "/wb ";
		if (text) then
			if(text:match("/%w+ ")) then
				chat = text:match("/%w+ ");
			end
		end
	end
	return chat;
end

function wbLeadHelper.onMouseOver()
	wbLeadHelper.onZoneMouseOver();
	wbLeadHelper.OnMouseOverCreateGroupStatsTooltip();

	local hoverWindow = tostring(SystemData.MouseOverWindow.name);
	local color = wbLeadHelper.Colors["grey"];
	WindowSetTintColor(hoverWindow, color[1], color[2], color[3]);
end

function wbLeadHelper.onMouseOut()
	wbLeadHelper.showNormalTitle();

	local hoverWindow = tostring(SystemData.MouseOverWindow.name);
	local color = wbLeadHelper.Colors["black"];	
	WindowSetTintColor(hoverWindow, color[1], color[2], color[3]);		
end

function wbLeadHelper.onZoneMouseOver()
	LabelSetText("wbLeadHelperWindowTitleLabel", L"wb <icon00092> / <icon00093> region");

	showingAlternateTitle = true;			
end

function wbLeadHelper.showNormalTitle()
	LabelSetText("wbLeadHelperWindowTitleLabel", L"wbLeadHelper");
end

function wbLeadHelper.OnMouseOverCreateGroupStatsTooltip()

	if subMenuActive then return end

	local selectedWindow = towstring(SystemData.ActiveWindow.name);
	local mouseoverMessageId = selectedWindow:match(L"wbLeadHelper_Message_(%d+)Background");
	mouseoverMessageId = tonumber(mouseoverMessageId);

	local item = wbLeadHelper.activeSettings.messages[mouseoverMessageId];

	if item.type ~= "LFM" then return end

	-- search for AUTO message with the submenu type "Parent"
	local groupCount = 8;
	for i=mouseoverMessageId+1, #wbLeadHelper.activeSettings.messages do
		if wbLeadHelper.activeSettings.messages[i].submenu == "Parent" then
			local match = wbLeadHelper.activeSettings.messages[i].message:match("<autoRoles(%d+)>");
			if (match) then
				groupCount = tonumber(match);
				break;
			end
		end
		--break when no more submenus directly after the clickedMessage are found
		if wbLeadHelper.activeSettings.messages[i].submenu == "None" or not wbLeadHelper.activeSettings.messages[i].submenu then
			break
		end
	end

	local stats = wbLeadHelper.AutoLookingForPlayers(groupCount);

	if not stats or stats == "2" then return end

	stats = wbLeadHelper.addCareerIcons(stats);

	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name )
    Tooltips.SetTooltipText( 1, 1, towstring(" Missing roles\n " .. stats .. " \n"))
    Tooltips.AnchorTooltip (Tooltips.ANCHOR_CURSOR)
    Tooltips.Finalize();
end

function wbLeadHelper.splitMessage(s, delimiter)
    local result = {};
    for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match);
    end
    return result;
end

function wbLeadHelper.trim(s)
   return (s:gsub("^%s*(.-)%s*$", "%1"))
end

function wbLeadHelper.getAllZoneNames()

	local zoneNames = {};
	table_insert(zoneNames, 1, "- BACK -");

	for i=1, #wbLeadHelper.ZoneIds do
		local zoneName = tostring(GetZoneName(wbLeadHelper.ZoneIds[i]));
		table_insert(zoneNames, zoneName);
	end

	return zoneNames;
end

function wbLeadHelper.getCountDowns()
	local countdowns = {}; 
	table_insert(countdowns, 1, "- BACK -");

	for i=1, #wbLeadHelper.CountdownSteps do
		table_insert(countdowns, tostring(wbLeadHelper.CountdownSteps[i]));
	end

	return countdowns;
end

function wbLeadHelper.getCurrentZone()
	return GameData.Player.zone;
end

function wbLeadHelper.getBObyZoneId(id)

	local bos = {};
	table_insert(bos, 1, "- BACK -");

	for i=1, #wbLeadHelper.BosByZoneId[id] do
		table_insert(bos, wbLeadHelper.BosByZoneId[id][i]);
	end

	for _, v in pairs(wbLeadHelper.MiscBoNames) do
		table_insert(bos, v)
	end
	
	return bos;
end

function wbLeadHelper.getSubmenuMessages(messageType)

	if clickedMainMenuMessageId < 1 then return end
	if not messageType then messageType = "message" end

	local messages = {}; 
	table_insert(messages, 1, "- BACK -");

	local clickedMessageType = wbLeadHelper.activeSettings.messages[clickedMainMenuMessageId].type;

	-- add messages with the submenu type "Parent"
	for i=clickedMainMenuMessageId+1, #wbLeadHelper.activeSettings.messages do
		if wbLeadHelper.activeSettings.messages[i].submenu == "Parent" then
			table_insert(messages, wbLeadHelper.activeSettings.messages[i][messageType]);
		end
		--break when no more submenus directly after the clickedMessage are found
		if wbLeadHelper.activeSettings.messages[i].submenu == "None" or not wbLeadHelper.activeSettings.messages[i].submenu then
			break
		end
	end
	
	-- add messages with the same type, no matter where they are located
	for i=1, #wbLeadHelper.activeSettings.messages do
		if wbLeadHelper.activeSettings.messages[i].submenu == clickedMessageType then
			table_insert(messages, wbLeadHelper.activeSettings.messages[i][messageType]);
		end
	end

	return messages;
end

function wbLeadHelper.toggleColoredChat()
	if not chatHookActive then
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> Permanent colored chat enabled.");
		chatHookActive = true;
	else
		Print("<LINK data=\"0\" text=\"[wbLeadHelper]\" color=\"50,255,10\"> Permanent colored chat disabled.");
		chatHookActive = false;
	end
end

function wbLeadHelper.OnKeyEnter(...)
	if not chatHookActive then
		return hookOnKeyEnter(...)
	end

	local message = wbLeadHelper.trim(tostring(EA_TextEntryGroupEntryBoxTextInput.Text));
	message = wbLeadHelper.activeSettings.messageStart .. " " .. message .. " " .. wbLeadHelper.activeSettings.messageEnd;	
	message = wbLeadHelper.ColorText(message, wbLeadHelper.Colors[wbLeadHelper.activeSettings.textColor]);

	EA_TextEntryGroupEntryBoxTextInput.Text = towstring(message);

	return hookOnKeyEnter(...);
end

function wbLeadHelper.ColorText(text, color)

	if ( not text ) then return end
	if ( not color ) then color = wbLeadHelper.Colors["white"] end

	-- Split sentences into single words so tehy can be colored and ignored seperately
	local words = wbLeadHelper.mysplit(text);

	--d(words)

	local newText = "";
	local coloringStarted = false;
	for i=1, #words do
		local word = words[i];
		if ( word == "" or word == nil or 
			 word:sub(1,1) == "/" or word:sub(1,1) == "." or 
			 word:sub(1,1) == "]" or word:sub(1,1) == "*" or 
			 word:sub(1,1) == "<") then

			-- end text coloring
			if(coloringStarted) then
				newText= newText .. "\">";
				coloringStarted = false;
			end

			-- add the string without color
			newText = newText .. word .. " ";
		elseif( word:sub(1,1) == "@") then
			-- end text coloring
			if(coloringStarted) then
				newText= newText .. "\">";
				coloringStarted = false;
			end

			local pl = word:gsub("@", "");
			newText = newText .. tostring(CreateHyperLink(L"PLAYER:" .. towstring(pl), towstring(word), {}, {} )) .. " ";
		else

			-- start text coloring
			if( not coloringStarted) then
				newText= newText .. "<LINK data=\"0\" color=\"" .. color[1] .. "," .. color[2] .. "," .. color[3] .. "\" text=\"";
				coloringStarted = true;
			end

			newText = newText .. word;
			
			-- end of array close coloring if open
			if( i == table.getn(words) and coloringStarted) then
				newText = newText .. "\">";
			end

			newText = newText .. " ";
		end	
	end	

	return newText;
end

function wbLeadHelper.mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={}
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                table.insert(t, str)
        end
        return t
end

function wbLeadHelper.AutoLookingForPlayers(roleCount)

	if not roleCount then roleCount = 8 end

	local groups = {};
	if wbLeadHelper.inWarBand() then
		groups = wbLeadHelper.gatherWarbandPlayers();
	else 
		groups = wbLeadHelper.gatherGroupPlayers();
	end

	--d(DUMP_TABLE(groups))

	if not groups then return end

	local tanks, healers, dpss = 0, 0, 0;
	for i=1, #groups do
		local grp = groups[i];
		--d(grp)
		for i=1, #grp do
			local player = grp[i];
			if (player.role == "tank") then
				tanks = tanks + 1
			end
			if (player.role == "healer") then
				healers = healers + 1
			end
			if (player.role == "mdps") then
				dpss = dpss + 1
			end
			if (player.role == "rdps") then
				dpss = dpss + 1
			end
		end
	end

	if (tanks == 0 and healers == 0 and dpss == 0) then return end
	if (tanks+healers+dpss == 24) then return "2" end

--[[	d("healers: " .. healers)
	d("tanks:   " .. tanks)
	d("dpss:    " .. dpss)
	d("total:   " .. tanks+healers+dpss)--]]

	if (tanks < roleCount or healers < roleCount or dpss < roleCount) then

		-- The following logic might search for more then 24 players in total until the warband is full
		-- This is by design to not limit the search to one specific role bit it prioritizes healers > tanks > dpss

		local healersStr = "";
		if healers < roleCount then
			local neededHeals = roleCount - healers;
			if ( tanks+healers+dpss+neededHeals >= 24) then
				neededHeals = neededHeals - (tanks+healers+dpss+neededHeals - 24);
				healers = healers + neededHeals;
			end
			healersStr = neededHeals .. " <healer> ";
			if ( (tanks < roleCount or dpss < roleCount) and ( tanks+healers+dpss < 24) ) then 
				healersStr = healersStr .. "/ ";
			end
		end

		local tanksStr = "";
		if tanks < roleCount and ( tanks+healers+dpss < 24) then
			local neededTanks = roleCount - tanks;
			if ( tanks+healers+dpss+neededTanks >= 24) then
				neededTanks = neededTanks - (tanks+healers+dpss+neededTanks - 24);
				tanks = tanks + neededTanks;
			end
			tanksStr = neededTanks .. " <tank> ";
			if (dpss < roleCount and ( tanks+healers+dpss < 24) ) then 
				tanksStr = tanksStr .. "/ ";
			end
		end

		local dpssStr = "";
		if dpss < roleCount and ( tanks+healers+dpss < 24) then
			local neededDpss = roleCount - dpss;
			if ( tanks+healers+dpss+neededDpss >= 24) then
				neededDpss = neededDpss - (tanks+healers+dpss+neededDpss - 24);
				dpss = dpss + neededDpss;
			end
			dpssStr = neededDpss .. " <dps>";
		end

		return healersStr .. tanksStr .. dpssStr;
	end
end

-- Gather warband data
-- This function was taken from the addon autoband with some modifcations
function wbLeadHelper.gatherWarbandPlayers()
	local group = {};
	local wbdata = GetBattlegroupMemberData();

	--wbdata = wbLeadHelper.FakeWbData;
	--wbdata = wbLeadHelper.FakeGroupData;
	--d(DUMP_TABLE(wbdata))

	for i in ipairs(wbdata) do
		group[i] = {};
	end

	for gid, grp in ipairs(wbdata) do
		for pid, player in ipairs(grp.players) do
			player.role = wbLeadHelper.getPlayerRoleByCareer(player.careerLine);
			table.insert(group[gid], player);
		end
	end

	return group;
end

-- Gather group data
function wbLeadHelper.gatherGroupPlayers()
	local group = {{}};
	--local gData = GroupWindow.groupData;
	local gData = PartyUtils.GetPartyData();

	--gData = wbLeadHelper.FakeGroupData2;
	--d(DUMP_TABLE(gData))

	-- Own player data
	local me = {
		["name"] = GameData.Player.name,
		["careerLine"] = GameData.Player.career.line,
		["role"] = wbLeadHelper.getPlayerRoleByCareer(GameData.Player.career.line);
		["isGroupLeader"] = GameData.Player.isGroupLeader,
	}
	table.insert(group[1], me);

	for pid, player in ipairs(gData) do
		if player.online and me.name ~= player.name then
			player.role = wbLeadHelper.getPlayerRoleByCareer(player.careerLine);
			table.insert(group[1], player);
		end
	end

	return group;
end

function wbLeadHelper.getWbLeader()
	local groups;
	if wbLeadHelper.inWarBand() then
		groups = wbLeadHelper.gatherWarbandPlayers();
	else 
		groups = wbLeadHelper.gatherGroupPlayers();
	end

	local leader = "";
	for i=1, #groups do
		local grp = groups[i];
		for i=1, #grp do
			local player = grp[i];
			if (player.isGroupLeader) then
				leader = tostring(player.name);
			end
		end
	end

	return leader;
end

function wbLeadHelper.getPlayerRoleByCareer(id)
	return wbLeadHelper.CareerLineById[id].type;
end

function wbLeadHelper.getOwnFactionCareerIconsByType()

	local TankIcons = "";
	local HealerIcons = "";
	local DpsIcons = "";

	for i=1, #wbLeadHelper.CareerLineById do
		local career = wbLeadHelper.CareerLineById[i];
		if(career.faction == GameData.Player.realm) then
			if (career.type == "tank") then
				TankIcons = TankIcons .. career.icon .. "";
			elseif (career.type == "healer") then
				HealerIcons = HealerIcons .. career.icon .. "";
			else
				DpsIcons = DpsIcons .. career.icon .. "";
			end
		end
	end

	return TankIcons, HealerIcons, DpsIcons;
end

function wbLeadHelper.setActiveSettings(settings)
	wbLeadHelper.activeSettings = settings;
end 

function wbLeadHelper.DefaultSettingsChangedDialog ()
	DialogManager.MakeTwoButtonDialog (
	L"wbLeadHelper has to be reset to it's default settings to function properly. This will reset all custom messages you created!\n\nDo it now?",
	L"Yes",
	function ()
		wbLeadHelper.Settings = wbLeadHelper.DefaultSettings;
		wbLeadHelper.setActiveSettings(wbLeadHelper.Settings)
		
		wbLeadHelper.createWbLeadHelperWindow()
		ModulesSaveSettings() -- Write SavedVariables.lua to disk
	end,
	L"No")
end

function wbLeadHelper.isNil (value, nilReturnValue)
	if (value == nil) then return nilReturnValue end
	return value
end

function wbLeadHelper.isEmpty (value, emptyReturnValue)
	if (not value or value:len() < 1) then return emptyReturnValue end
	return value
end

function wbLeadHelper.indexOf(array, value)
    for i, v in ipairs(array) do
        if v == value then
            return i
        end
    end
    return nil
end

function wbLeadHelper.inWarBand()
    return IsWarBandActive()
end

function wbLeadHelper.inAParty()
    return GetNumGroupmates() > 0
end
