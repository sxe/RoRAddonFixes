A simple warhammer online addon which makes it easier for warband and siege leaders to communicate with their followers by broadcasting text sequences into chat. 

Functionality:

	* type /wlh show to open the addon
	* type /wlh config to open the addons configuration dialog
	
	* Leftclick on a message label to broadcast the message to the warband (/wb) channel
	* Rightclick on a message label to broadcast the message to the region (/1) channel

	* Click on a countdown label to start a countdown with the noted time, click it again to abort the countdown.

	* Click on a "ZONE" message label to dynamically select a zone. $Z in a message text will be replaced with the selected zone.

	* Click on a "LFM" message to specify the role you are looking for or choose AUTO mode to search for missing roles based on your current warband automatically.

----------------

Installation:

	* Get it from here: https://tools.idrinth.de/addons/wbleadhelper/

-----------------
0.9.7 - Updated as of 7/05/2022
* Fixed 2/2/2 auto search

0.9.6 - Updated as of 5/26/2022
* Chat channels can now be set in each message
* Bugfixes

0.9.5
* The LFG submenu is now editable as well.
* The AUTO group scan feature got more flexible. You can now define the number of missing roles you are looking for (Good for none warbands).
* Added help tooltips in more places.

0.9.1
* A tooltip with needed roles is now shown when hovering the *LFM* button.
* Line breaks in message labels are now done with <nl> not <br> anymore
* Some smaller bug fixes

0.9 - Updated as of 5/06/2022.
* A full config UI was added. Messages can now be added and changed from withing the game.
* Added AUTO looking for players search. It scans your warband and puts out a message with missing roles (healers, tanks, dps)
* Messages are in different colors now
* Reorderd the message labels to make them more intuitive to use
* Some small adjustments

0.7 - Updated as of 7/22/2020.
* Added "Color Mode", to be able to chat colored all the time.

0.6.5 - Updated as of 7/16/2020.
* Button text can now be colored
* Added "back" button to submenus
* Realigned text in buttons to better fit
* Submenus are now better recognizable

0.6 - Updated as of 7/13/2020.
* Added button to shout out a BO 
* Added a "Looking for Members" button which post to the /5 LFG channel on left click. Or to /1 on right click.
* Some bug fixes and code optimizations

0.5 - Updated as of 6/24/2020.
* Reworked countdown functionality

0.4 - Updated as of 6/24/2020.
* Improved window scaling
* Added more messages
* Added a new layout (two button rows) 
* Added mouse over button color change 

0.3 - Updated as of 6/06/2020.
* Added a Zone announcement message

0.2.3 - Updated as of 6/04/2020.
* Added a variable at the top of the document to make it possible to change the size of the window

0.2.2 - Updated as of 6/03/2020.
* Added a variable at the top of the document to make changing the text color easier

0.2 - Updated as of 6/02/2020.
* Initial Release